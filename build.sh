#!/bin/bash
# create new image from alpine linux mini fs image
#
# Image version will as same as alpine base image.
#
# usage 
# 	./build.sh [tag] [version]
# 
# - tag: Target docker image tag name
# - version: Alpine linux version 
# 
# Example 
#
# 	./build.sh bytekode/alpine-mrfs 3.7.0


# Function print ussage use for guide when error occur
function printUsage() {
	printf "\n\nUsage:\n"
	printf "\n\t$0 [tag] [version]\n"
	printf "\n\t\t- tag: Target docker image tag name."
	printf "\n\t\t- version: Alpine linux version. "

	printf "\n\nExample\n\n"
	printf "\t$0 bytekode/alpine-mrfs 3.7.0"
	printf "\n\n"
}

# Step 1: Validate script parameters
if [ $# -lt 2 ]; then
	printf "\n[ERROR] Missing required parameters."
	printUsage
	exit 1
fi 

# Step 2: Assign parameters to variables
TAG=$1
VERSION=$2

VERSION_MAJOR=$(echo $VERSION|awk -F "." '{print $1}')
VERSION_MINOR=$(echo $VERSION|awk -F "." '{print $2}')
VERSION_REVISION=$(echo $VERSION|awk -F "." '{print $3}')
VERSION_MM=v${VERSION_MAJOR}.${VERSION_MINOR}
ARCH="x86_64"

OS_IMAGE=alpine-minirootfs-${VERSION}-${ARCH}.tar.gz
OS_IMAGE_SHA256=${OS_IMAGE}.sha256
CONTAINER_NAME=alpine_minirootfs_builder


CUR_DIR=$(pwd)

# Step 3: Download Alpine linux image file from official site.
if [ ! -f ${CUR_DIR}/${OS_IMAGE_SHA256} ]; then
	printf "\nDownloading Alpine Linux Image...\n"
	wget http://dl-cdn.alpinelinux.org/alpine/${VERSION_MM}/releases/${ARCH}/${OS_IMAGE_SHA256}
	wget http://dl-cdn.alpinelinux.org/alpine/${VERSION_MM}/releases/${ARCH}/${OS_IMAGE}

	if [ $? -gt 0 ]; then 
		printf "[ERROR] Unable to download Archlinux version ${VERSION} from official site. "
		printf "\n[ERROR] Please check internet connection or version number is correct.\n\n"

		exit 1;
	fi
fi

# Step 4: Checksum images
printf "\nVerifying downloaded file with sha256.."
sha256sum -c $OS_IMAGE_SHA256

if [ $? -eq 0 ]
then 
	printf "File verification OK"
else
	# Step 4.1: Verification failed. Delete all download file and download again
	rm -f *.tar.gz*
	wget http://dl-cdn.alpinelinux.org/alpine/${VERSION_MM}/releases/${ARCH}/${OS_IMAGE_SHA256}
	wget http://dl-cdn.alpinelinux.org/alpine/${VERSION_MM}/releases/${ARCH}/${OS_IMAGE}
	if [ $? -gt 0 ]; then 
		printf "[ERROR] Unable to download Archlinux version ${VERSION} from official site. "
		printf "\n[ERROR] Please check internet connection or version number is correct.\n\n"

		exit 1;
	fi

	# Checksum images
	sha256sum -c $OS_IMAGE_SHA256
	if [ $? -gt 0 ]
	then 
		printf "[ERROR]File verification FAILED\n\n"
		exit 1
	fi
fi

# Step 5: Creating docker image from os image
chmod u+x ${CUR_DIR}/${OS_IMAGE}

printf "\nDeleting current image..."
docker ps -a | awk '{ print $1,$2 }' | grep ${TAG} | awk '{print $1 }' | xargs -I {} docker rm {}
docker rmi ${TAG} > /dev/null

printf "\n\nCreating docker base image from alpine-minirootfs-${VERSION}-${ARCH}..\n"
docker import alpine-minirootfs-${VERSION}-${ARCH}.tar.gz ${TAG} > /dev/null

docker run -d --name ${CONTAINER_NAME} ${TAG} /bin/sh > /dev/null

docker commit ${CONTAINER_NAME} ${TAG}:${VERSION} > /dev/null
docker tag ${TAG}:${VERSION} ${TAG}:latest

# Show result of created image
docker images ${TAG}:${VERSION}

# Step 6: Clear unused stuff
docker rm -f ${CONTAINER_NAME} > /dev/null
rm -f ${CUR_DIR}/${OS_IMAGE} ${CUR_DIR}/${OS_IMAGE_SHA256}
# Delete untagged images
N_UNTAGGED=$(docker images | grep "^<none>" | wc -l)
if [ $N_UNTAGGED -gt 0 ]; then
	docker rmi -f $(docker images | grep "^<none>" | awk '{print $3}') > /dev/null
fi

# Alpine mini root filesystem already created 
printf "\nAlpine base image successful created.\n\n"

# Creating alpine-glibc image

GLIBC_VERSION=$( curl -s https://github.com/sgerrand/alpine-pkg-glibc/releases/latest | awk -F "body" '{print $2}' | awk -F '"' '{print $2}' | awk -F "/" '{print $NF}')

# Detect proxy from variable http_proxy
PROXY=$(echo $HTTP_PROXY $http_proxy $HTTP_PROXY $HTTPS_PROXY $https_proxy | awk '{print $1}')

# printf "${GLIBC_VERSION}"

docker build --build-arg GLIBC_VERSION=$GLIBC_VERSION --build-arg PROXY=$PROXY -t ${TAG}-glibc:${VERSION} -t ${TAG}-glibc:latest .

# Check result of docker build 
if [ $? -gt 0 ]
	then 
		printf "[ERROR]Docker build failed.\n\n"
		exit 1
	fi

# Show result of created image
# docker images ${TAG}-glibc:${VERSION}

# Alpine mini root filesystem already created 
printf "\nAlpine glibc image successful created.\n\n"

