FROM bytekode/alpine:latest

ARG PROXY
ARG GLIBC_VERSION

ENV LANG=en_US.UTF-8

RUN export HTTP_PROXY=${PROXY} && \
    export HTTPS_PROXY=${PROXY} && \
    \
    apk update && \
    apk add --no-cache --virtual=.build-dependencies \
	ca-certificates \
	wget \
	tzdata && \
    update-ca-certificates && \
    \
    echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf && \ 
    \
    export http_proxy=${PROXY} && \
    export https_proxy=${PROXY} && \
    wget https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -O /etc/apk/keys/sgerrand.rsa.pub && \
    wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk  \
    	 https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-bin-${GLIBC_VERSION}.apk  \
    	 https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-i18n-${GLIBC_VERSION}.apk && \
    \
    apk add --no-cache \
        glibc-${GLIBC_VERSION}.apk \
        glibc-bin-${GLIBC_VERSION}.apk \
        glibc-i18n-${GLIBC_VERSION}.apk && \
    \ 
    /usr/glibc-compat/bin/localedef -i en_US -f UTF-8 en_US.UTF-8 && \
    cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && \
    \ 
    rm /etc/apk/keys/sgerrand.rsa.pub && \
    apk del glibc-i18n && \
    rm \
        glibc-${GLIBC_VERSION}.apk \
        glibc-bin-${GLIBC_VERSION}.apk \
        glibc-i18n-${GLIBC_VERSION}.apk && \
    apk del .build-dependencies && \
    rm -rf /var/cache/apk/* /tmp/* /var/tmp/* && \
    rm /root/.wget-hsts 
