# Alpine Linux with GLIBC

This project will be build 2 images
	
	- bytekode/alpine 
	- bytekode/alpine-glibc

Glibc are required by Java base application.

Latest release are available on  [Github](https://github.com/sgerrand/alpine-pkg-glibc/releases)

## Usage

```bash
 	./build.sh [tag] [version]
```
 **Parameters**
 - tag: Target docker image tag name
 - version: Alpine linux version 
 
## Example 

**Buid with shell script**
```bash
 	./build.sh bytekode/alpine 3.8.0
```

**Build with gitlab runner**
```
gitlab-runner exec shell  build
```

